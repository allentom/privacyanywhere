from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('list', views.list, name='list'),
    path('start', views.start, name='start'),
    path('<int:q_id>/', views.detail, name='detail'),
    path('<int:q_id>/results/', views.results, name='results'),
    path('<int:q_id>/enter/', views.enter, name='enter'),
    path('success', views.save_response, name='save_response'),
    path('continue/<int:survey_id>', views.survey, name='survey'),
    path('explain', views.explain, name='explain'),
   
]

