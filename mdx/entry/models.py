from django.db import models


RESPONSE_CHOICES = (
    ('no','No'),
    ('yes', 'Yes'),
)

class Question(models.Model):
    readonly_fields=('id')
    text = models.CharField(max_length=200)
    created = models.DateTimeField(auto_now_add=True)
    is_noise = models.BooleanField(default=True)

    def __str__(self):
        return str(self.text)

    def get_choices(self):
        return self.choice_set.all()

class Choice(models.Model):
    readonly_fields=('id')
    text = models.CharField(max_length=200)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"question: {self.question.text}, choice: {self.text}"


class Survey(models.Model):
    epsilon = models.FloatField(default=4.0)
    accuracy = models.IntegerField(default=80)
    noise = models.IntegerField(default=20)
    created = models.DateTimeField(auto_now_add=True)    
    def __str__(self):
        return str(self.id)

class Response(models.Model):
    readonly_fields=('id')
    text = models.CharField(max_length=6, choices=RESPONSE_CHOICES, default='yes')
    created = models.DateTimeField(auto_now_add=True)
    survey = models.ForeignKey(Survey,related_name="responses", on_delete=models.CASCADE)
    def __str__(self):
        return f"response: {self.id}"




   
