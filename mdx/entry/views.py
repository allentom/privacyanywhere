from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from django.http import Http404
from .forms import *
import requests
import random
import json

from .models import *
# Create your views here.


def index(request):
    return render(request, 'entry/index.html')

def explain(request):
    return render(request, 'entry/explain.html')

# the sets up a survey, the user sets the desired accuracy level required for the survey
# the related noise, epsilon values are computed alongside
def start(request):
    form = SurveyForm()
    if request.method == 'POST':
        form = SurveyForm(request.POST)
        if form.is_valid():
            instance = form.save()
            survey_record = Survey.objects.get(id = instance.id)
            survey_record.noise = (100-survey_record.accuracy)
            if survey_record.noise <= 0:
                survey_record.noise = 1
            if survey_record.accuracy > 100:
                survey_record.accuracy = 100

            survey_record.epsilon = round(survey_record.accuracy/survey_record.noise, 2)
            survey_record.save()
            return survey(request, survey_id = instance.id)
    context = {'form': form}
    return render(request, 'entry/start.html', context)

def survey(request, survey_id):
    #retreive details of survey
    survey_record = Survey.objects.get(id = survey_id)
    
    #decide if user should get get noise question or not
    accuracy_needed = accuracyornoise(survey_record.accuracy)
    question_record = Question.objects.get(is_noise = accuracy_needed.get("result"))
    accuracy_needed['survey_id'] = survey_id
    return detail(request, question_record.id, accuracy_needed)


# this function is like a virtual coin flip, 
# it decides if the question should be one that gives a noise response or an accurate one by comparing 
# the accuracy value needed in the survey with a random number
def accuracyornoise(accuracy):

    # we generate a random value using a public API that shows numbers from atmospheric noise
    resource_url = 'https://www.random.org/integers/'
    params = {"num": 1, "min": 1, "max":100, "col":1, "base":10,
             "format":"plain", "rnd": "new"}
    request_for_naturalrandom = requests.get(resource_url, params)
   
    if request_for_naturalrandom.status_code == 200:
        generated_random = int(request_for_naturalrandom.text)
    else:
        # fall back in case Randomizing API is not available
        generated_random = random.randint(0, 100)

    # Decide if the question shown should be a noise question or real one
    if accuracy <= generated_random:
        add_noise = True
    else:
        add_noise = False

    noise = (100-accuracy)

    if noise <= 0:
        noise = 1
    if accuracy > 100:
        accuracy = 100
    
    # from differential privacy formula
    epsilon = round(accuracy/noise, 2)

    # return computated values to the application to display the question
    return {'accuracy': accuracy, 'noise': noise, 'epsilon': epsilon, 
            'randomized_number': generated_random, 'result': add_noise}



def detail(request, q_id, accuracy):

    form = ResponseForm(initial={'survey': accuracy['survey_id']})

    try:
        question = Question.objects.get(pk=q_id)
    except Question.DoesNotExist:
        raise Http404("Question does not exist")
    return render(request, 'entry/detail.html', {'form': form, 'question': question, 'accuracy': accuracy})

def save_response(request):
    if request.method == 'POST':
        form = ResponseForm(request.POST)
    if form.is_valid():
        instance = form.save()

    survey_id = form.cleaned_data['survey']
    log = json_report(survey_id.id)
    
    return render(request, 'entry/response_complete.html', {'survey_id':form.cleaned_data['survey'], 'response_id':instance.id, 'log':log})

def json_report(survey_id):
    survey = Survey.objects.get(pk=survey_id)
    yes_count = Response.objects.filter(survey=survey.id, text = 'yes').count()
    no_count = Response.objects.filter(survey=survey.id, text = 'no').count()
    responses = (yes_count+no_count)
    log = {"id":survey.id, "accuracy":survey.accuracy, "noise": survey.noise, "epsilon":survey.epsilon, "yes": yes_count, "no":no_count, "total_responses":responses}
    return json.dumps(log)

def results(request, q_id):
    response = "Result of Question %s."
    return HttpResponse(response % q_id)

def enter(request, q_id):
    return HttpResponse("Answering: %s." % q_id)

def list(request):
    latest_question_list = Question.objects.order_by('-created')[:5]
    template = loader.get_template('entry/index.html')
    output = ', '.join([q.text for q in latest_question_list])
    #return HttpResponse(output)
    return render(request, 'entry/list.html', {'question': output})
