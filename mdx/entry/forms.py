from django.forms import ModelForm
from .models import Survey, Response

# Create the form class.
class SurveyForm(ModelForm):
    class Meta:
        model = Survey
        fields = '__all__'

class ResponseForm(ModelForm):
    class Meta:
        model = Response
        fields = '__all__'