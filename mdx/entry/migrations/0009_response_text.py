# Generated by Django 3.2.3 on 2021-10-16 02:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entry', '0008_response'),
    ]

    operations = [
        migrations.AddField(
            model_name='response',
            name='text',
            field=models.CharField(choices=[('no', 'No'), ('yes', 'Yes')], default='yes', max_length=6),
        ),
    ]
