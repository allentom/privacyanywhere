# Generated by Django 3.2.3 on 2021-10-16 02:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('entry', '0006_auto_20211016_0237'),
    ]

    operations = [
        migrations.CreateModel(
            name='Survey',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('epsilon', models.IntegerField(default=3)),
                ('accuracy', models.IntegerField(default=80)),
                ('noise', models.IntegerField(default=20)),
                ('created', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
